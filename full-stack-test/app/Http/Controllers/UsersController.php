<?php

namespace App\Http\Controllers;

use App\Models\Users;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
    public function get_all_user()
    {
        $users = Users::all();
        return response()->json([
            'users' => $users,
        ], 200);
    }

    public function add_user(Request $request)
    {
        $user = new Users();

        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->save();

        return response()->json([
            'message' => 'Usuário criado com sucesso',
        ], 201);
    }

    public function get_edit_user($id)
    {
        $user = Users::find($id);
        return response()->json([
            'user' => $user,
        ], 200);
    }

    public function update_user(Request $request, $id)
    {
        $user = Users::find($id);

        $user->name = $request->name;
        $user->email = $request->email;
        $user->save();

        return response()->json([
            'message' => 'Usuário atualizado com sucesso',
        ], 200);
    }

    public function delete_user($id)
    {
        $user = Users::findOrFail($id);
        $user->delete();

        return response()->json([
            'message' => 'Usuário excluído com sucesso',
        ], 200);
    }

    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials, true)) {
            $user = Auth::user();
            $token = $user->createToken('auth-token')->plainTextToken;

            return response()->json([
                'user' => $user,
                'token' => $token,
            ], 200);
        } else {
            return response()->json([
                'message' => 'Credenciais inválidas',
            ], 401);
        }
    }
}
