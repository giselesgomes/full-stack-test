import React, { useState, useEffect } from 'react';
import Navbar from '../../components/Navbar/Navbar';
import GenreCarousel from '../../components/GenreCarousel/GenreCarousel';
import Footer from '../../components/Footer/Footer'
import { Title, Content } from './Home.style';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

interface Movie {
    id: number;
    title: string;
    name: string;
    poster_path: string;
    genre_ids: number[];
}

interface Genre {
    id: number;
    name: string;
}

const Home: React.FC = () => {
    const [movies, setMovies] = useState<Movie[]>([]);
    const [genres, setGenres] = useState<Genre[]>([]);

    useEffect(() => {
        // filmes
        fetch('https://api.themoviedb.org/3/movie/popular?api_key=07e3d98271fb0c634776f4a8bd83b483&language=en-US&page=1')
            .then(response => response.json())
            .then(data => setMovies(data.results));

        // gêneros
        fetch('https://api.themoviedb.org/3/genre/movie/list?api_key=07e3d98271fb0c634776f4a8bd83b483&language=en-US')
            .then(response => response.json())
            .then(data => setGenres(data.genres));
    }, []);

    useEffect(() => {
        const fetchMovies = async () => {
            const totalPages = 5;
            const movieSet = new Set<number>();


            for (let page = 1; page <= totalPages; page++) {
                const response = await fetch(
                    `https://api.themoviedb.org/3/trending/all/week?api_key=07e3d98271fb0c634776f4a8bd83b483&language=en-US&page=${page}`
                );
                const data = await response.json();
                const newMovies = data.results.filter((movie: Movie) => {
                    // verifica filme
                    if (movieSet.has(movie.id)) {
                        return false;
                    } else {
                        movieSet.add(movie.id);
                        return true;
                    }
                });

                // Concatena os filmes da página atual aos filmes existentes
                setMovies(prevMovies => [...prevMovies, ...newMovies]);
            }
        };

        fetchMovies();
    }, []);


    // ficção cientifica
    const filteredFiction = movies.filter(movie => movie.genre_ids.includes(878));

    // filmes de ação
    const filteredAction = movies.filter(movie => movie.genre_ids.includes(28));

    // filmes de terror
    const filteredHorror = movies.filter(movie => movie.genre_ids.includes(27));

    // filmes de comedia
    const filteredComedy = movies.filter(movie => movie.genre_ids.includes(35));

    return (
        <div>
            <header>
                <Navbar />
            </header>
            <Content>
                <section>
                    <Title>Bem vindo!</Title>
                </section>
                {movies.length > 0 ? (
                    <>
                        <GenreCarousel movies={filteredFiction} genres={genres} title="Animação" carouselId="carousel1" />
                        <GenreCarousel movies={filteredAction} genres={genres} title="Ação" carouselId="carousel2" />
                        <GenreCarousel movies={filteredHorror} genres={genres} title="Terror" carouselId="carousel2" />
                        <GenreCarousel movies={filteredComedy} genres={genres} title="Comédia" carouselId="carousel2" />
                    </>
                ) : (
                    <p>Carregando filmes...</p>
                )}
            </Content>
            <Footer />
        </div>
    );
};

export default Home;
