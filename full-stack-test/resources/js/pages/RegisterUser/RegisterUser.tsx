import React, { useState } from 'react';
import { Container, Form, InputGroup, Label, Input, ErrorMessage, Button } from './RegisterUser.style';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import Footer from '../../components/Footer/Footer';

interface FormValues {
    name: string;
    email: string;
    password: string;
    confirmPassword: string;
}

const RegisterUser: React.FC = () => {
    const navigate = useNavigate();

    const [name, setName] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [confirmPassword, setConfirmPassword] = useState("");

    const [errors, setErrors] = useState<FormValues>({
        name: '',
        email: '',
        password: '',
        confirmPassword: '',
    });

    const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const { name, value } = e.target;
        setErrors(prevErrors => ({
            ...prevErrors,
            [name]: '',
        }));

        if (name === 'name') {
            setName(value);
        } else if (name === 'email') {
            setEmail(value);
        } else if (name === 'password') {
            setPassword(value);
        } else if (name === 'confirmPassword') {
            setConfirmPassword(value);
        }
    };

    const handleSubmit = (e: React.FormEvent) => {
        e.preventDefault();
        if (validateForm()) {
            createUser();
        }
    };

    const validateForm = () => {
        let isValid = true;
        const newErrors: FormValues = {
            name: '',
            email: '',
            password: '',
            confirmPassword: '',
        };

        if (!name) {
            isValid = false;
            newErrors.name = 'Por favor, insira um nome.';
        }

        if (!email) {
            isValid = false;
            newErrors.email = 'Por favor, insira um e-mail.';
        } else if (!isValidEmail(email)) {
            isValid = false;
            newErrors.email = 'Por favor, insira um e-mail válido.';
        }

        if (!password) {
            isValid = false;
            newErrors.password = 'Por favor, insira uma senha.';
        } else if (password.length < 6) {
            isValid = false;
            newErrors.password = 'A senha deve ter pelo menos 6 caracteres.';
        }

        if (password !== confirmPassword) {
            isValid = false;
            newErrors.confirmPassword = 'As senhas não coincidem.';
        }

        setErrors(newErrors);
        return isValid;
    };

    const isValidEmail = (email: string) => {
        const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
        return emailRegex.test(email);
    };

    const createUser = async () => {
        const formData = new FormData();

        formData.append('name', name);
        formData.append('email', email);
        formData.append('password', password);

        try {
            const { data } = await axios.post("/api/add_user", formData);
            alert("Usuário cadastrado com sucesso!");
            console.log("Usuário cadastrado");
            navigate("/");
        } catch (error) {
            console.error("Erro ao cadastrar usuário");
        }
    };

    return (
        <>
            <Container>
                <h1>Cadastro</h1>
                <Form onSubmit={handleSubmit}>
                    <InputGroup>
                        <Label>Nome:</Label>
                        <Input
                            type="text"
                            name="name"
                            value={name}
                            onChange={handleChange}
                        />
                        {errors.name && <ErrorMessage>{errors.name}</ErrorMessage>}
                    </InputGroup>
                    <InputGroup>
                        <Label>E-mail:</Label>
                        <Input
                            type="email"
                            name="email"
                            value={email}
                            onChange={handleChange}
                        />
                        {errors.email && <ErrorMessage>{errors.email}</ErrorMessage>}
                    </InputGroup>
                    <InputGroup>
                        <Label>Senha:</Label>
                        <Input
                            type="password"
                            name="password"
                            value={password}
                            onChange={handleChange}
                        />
                        {errors.password && <ErrorMessage>{errors.password}</ErrorMessage>}
                    </InputGroup>
                    <InputGroup>
                        <Label>Repetir Senha:</Label>
                        <Input
                            type="password"
                            name="confirmPassword"
                            value={confirmPassword}
                            onChange={handleChange}
                        />
                        {errors.confirmPassword && (
                            <ErrorMessage>{errors.confirmPassword}</ErrorMessage>
                        )}
                    </InputGroup>
                    <Button type="submit">Cadastrar</Button>
                </Form>
            </Container>
        </>
    );
};

export default RegisterUser;
