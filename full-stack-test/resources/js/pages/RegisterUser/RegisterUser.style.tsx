import styled from 'styled-components';
import colors from '../../variables/Colors';

export const Container = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    padding: 2rem;

    h1 {
        margin-bottom: 22px;
    }
`;

export const Form = styled.form`
    display: flex;
    flex-direction: column;
    width: 300px;
`;

export const InputGroup = styled.div`
    display: flex;
    flex-direction: column;
    margin-bottom: 20px;
`;

export const Label = styled.label`
    margin-bottom: 5px;
`;

export const Input = styled.input`
    padding: 10px;
    border: 1px solid #ccc;
    border-radius: 4px;
    font-size: 16px;
`;

export const ErrorMessage = styled.p`
    background: ${colors.error_color};
    margin: 12px 0;
    padding: 4px 8px;
    border-radius: 8px;
    font-size: 12px;
`;

export const Button = styled.button`
    padding: 10px;
    background-color: ${colors.main_color};
    color: #fff;
    border: none;
    border-radius: 4px;
    font-size: 16px;
    cursor: pointer;
`;