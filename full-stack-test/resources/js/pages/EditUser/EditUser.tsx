import React, { useState, useEffect } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { Container, Form, InputGroup, Label, Input, ErrorMessage, Button } from '../RegisterUser/RegisterUser.style';
import axios from 'axios';

interface FormValues {
    name: string;
    email: string;
}

const EditUser: React.FC = () => {
    const navigate = useNavigate();
    const { id } = useParams();

    const [name, setName] = useState("");
    const [email, setEmail] = useState("");
    const [formValues, setFormValues] = useState<FormValues>({
        name: '',
        email: '',
    });
    const [errors, setErrors] = useState<FormValues>({
        name: '',
        email: '',
    });

    useEffect(() => {
        getUsers();
    }, []);

    const getUsers = async () => {
        try {
            const { data } = await axios.get(`/api/get_edit_user/${id}`);
            const { name, email } = data.user;
            setName(name);
            setEmail(email);
            setFormValues({ name, email });
        } catch (error) {
            console.error("Erro ao obter dados do usuário");
        }
    };

    const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const { name, value } = e.target;
        setFormValues(prevState => ({
            ...prevState,
            [name]: value,
        }));
    };

    const handleSubmit = (e: React.FormEvent) => {
        e.preventDefault();
        if (validateForm()) {
            updateUser();
        }
    };

    const validateForm = () => {
        let isValid = true;
        const newErrors: FormValues = {
            name: '',
            email: '',
        };

        if (!formValues.name) {
            isValid = false;
            newErrors.name = 'Por favor, insira um nome.';
        }

        if (!formValues.email) {
            isValid = false;
            newErrors.email = 'Por favor, insira um e-mail.';
        } else if (!isValidEmail(formValues.email)) {
            isValid = false;
            newErrors.email = 'Por favor, insira um e-mail válido.';
        }

        setErrors(newErrors);
        return isValid;
    };

    const isValidEmail = (email: string) => {
        // Lógica para validar o formato do e-mail
        const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
        return emailRegex.test(email);
    };

    const updateUser = async () => {
        const formData = new FormData();
        formData.append('name', formValues.name);
        formData.append('email', formValues.email);

        try {
            await axios.post(`/api/update_user/${id}`, formData);
            alert("Usuário editado com sucesso!");
            console.log("Usuário editado");
            navigate("/users");
        } catch (error) {
            console.error("Erro ao editar usuário");
        }
    };

    return (
        <>
            <Container>
                <h1>Editar usuário:</h1>
                <Form onSubmit={handleSubmit}>
                    <InputGroup>
                        <Label>Nome:</Label>
                        <Input
                            type="text"
                            name="name"
                            value={formValues.name}
                            onChange={handleChange}
                        />
                        {errors.name && <ErrorMessage>{errors.name}</ErrorMessage>}
                    </InputGroup>
                    <InputGroup>
                        <Label>E-mail:</Label>
                        <Input
                            type="email"
                            name="email"
                            value={formValues.email}
                            onChange={handleChange}
                        />
                        {errors.email && <ErrorMessage>{errors.email}</ErrorMessage>}
                    </InputGroup>

                    <Button type="submit">Atualizar</Button>
                </Form>
            </Container>
        </>
    );
};

export default EditUser;
