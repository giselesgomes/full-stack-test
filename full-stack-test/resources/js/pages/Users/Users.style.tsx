import styled from 'styled-components';

export const Container = styled.section`
    width: 100%;
    margin: 0 auto;
    display: flex;
    flex-direction: column;

    @media (min-width: 1024px) {
        width: 80%;
    }
`;

export const Header = styled.div`
    h1 {
    text-align: center;
    margin: 32px 0;
    }
`;

export const Table = styled.table`
    margin: 0 auto;

    @media (min-width: 1024px) {
        width: 100%;
    }

`;

export const TableRow = styled.tr`

`;

export const TableHead = styled.thead`
    
`;

export const TableData = styled.td`
    padding: 10px;
    border: 1px solid #ddd;
`;

export const ButtonNew = styled.button`
    width: 130px;
    padding: 5px 10px;
    background-color: #6a994e;
    color: #fff;
    border: none;
    border-radius: 3px;
    cursor: pointer;
    margin: 0 0 12px 12px;
    display: flex;
    column-gap: 6px;
    align-items: center;

    @media (min-width: 1024px) {
        margin: 0 0 12px 0;
    }


    &:hover {
    background-color: #386641;
    }
`;

export const ButtonEdit = styled.button`
    padding: 5px 10px;
    background-color: #ffbc42;
    color: #fff;
    border: none;
    border-radius: 3px;
    cursor: pointer;
    margin-bottom: 12px;
    display: flex;
    column-gap: 6px;
    align-items: center;

    span {
        font-size: 0;

        @media (min-width: 1024px) {
            font-size: 1rem;
        }
    }

    &:hover {
    background-color: #ee964b;
    }
`;

export const ButtonDelete = styled.button`
    padding: 5px 10px;
    background-color: #e5383b;
    color: #fff;
    border: none;
    border-radius: 3px;
    cursor: pointer;
    margin-bottom: 12px;
    display: flex;
    column-gap: 6px;
    align-items: center;

    span {
        font-size: 0;

        @media (min-width: 1024px) {
            font-size: 1rem;
        }
    }

    &:hover {
    background-color: #ba181b;
    }
`;