import React, { useEffect, useState } from 'react';
import { Container, Table, TableRow, TableHead, TableData, Header, ButtonNew, ButtonEdit, ButtonDelete } from './Users.style';
import { useNavigate } from 'react-router-dom';
import Navbar from '../../components/Navbar/Navbar';
import axios from 'axios';
import { LuPlusCircle, LuEdit, LuXSquare } from "react-icons/lu";

interface User {
    id: number;
    name: string;
    email: string;
}

interface UserTableProps {
    users: User[];
    onDelete: (userId: number) => void;
    onEdit: (userId: number) => void;
}

const Users: React.FC = () => {
    const navigate = useNavigate();
    const [users, setUsers] = useState<User[]>([]);

    useEffect(() => {
        getUsers();
    }, []);

    const getUsers = async () => {
        try {
            const response = await axios.get("/api/get_all_user");
            setUsers(response.data.users);
        } catch (error) {
            console.log(error);
        }
    };

    const newUser = () => {
        navigate("/register");
    };

    const editUser = (id: number) => {
        navigate('/users/edit/' + id);
    };

    const deleteUser = (id: number) => {
        if (window.confirm("Deseja deletar usuário?")) {
            axios.get('/api/delete_user/' + id)
                .then(() => {
                    alert('Usuário deletado!');
                    getUsers();
                })
                .catch(() => {
                    alert('Erro ao deletar usuário.');
                });
        }
    };

    return (
        <>
            <Navbar />
            <Header>
                <h1>Usuários</h1>
            </Header>
            <Container>
                <ButtonNew onClick={newUser}>
                    <LuPlusCircle />
                    <span>Novo usuário</span>
                </ButtonNew>

                <Table>
                    <TableHead>
                        <TableRow>
                            <TableData>Nome</TableData>
                            <TableData>E-mail</TableData>
                            <TableData>Ações</TableData>
                        </TableRow>
                    </TableHead>
                    <tbody>
                        {users.length > 0 &&
                            users.map((item) => (
                                <TableRow key={item.id}>
                                    <TableData>{item.name}</TableData>
                                    <TableData>{item.email}</TableData>
                                    <TableData>
                                        <ButtonEdit onClick={() => editUser(item.id)}>
                                            <LuEdit />
                                            <span>Editar</span>
                                        </ButtonEdit>
                                        <ButtonDelete onClick={() => deleteUser(item.id)}>
                                            <LuXSquare />
                                            <span>Excluir</span>
                                        </ButtonDelete>
                                    </TableData>
                                </TableRow>
                            ))}
                    </tbody>
                </Table>
            </Container>
        </>
    );
};

export default Users;
