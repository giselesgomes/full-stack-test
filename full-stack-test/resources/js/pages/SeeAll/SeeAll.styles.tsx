import styled from 'styled-components';
import colors from '../../variables/Colors';

export const Container = styled.section`
    padding: 2rem; 
`;

export const MovieList = styled.ul`
    display: grid;
    grid-template-columns: repeat(auto-fit, minmax(250px, 1fr));
    grid-gap: 20px;
    list-style: none;
    padding: 0;
`;

export const MovieItem = styled.li`
    display: flex;
    flex-direction: column;
    align-items: center;
    text-align: center;
    padding: 10px;
    
    img {
        width: 100%;
        height: auto;
        margin-bottom: 10px;
        border-radius: 1rem;
    }

    h3 {
    margin-bottom: 5px;
    min-height: 50px;

    }

    div {
        display: flex;
        column-gap: 14px;
        justify-content: center;
        padding-bottom: 12px;


        button {
            background: none;
            border: none;
            font-size: 1rem;
            color: #FFF;
            font-weight: 600;

        }

        p {
            color: ${colors.main_color};
            font-weight: 600;
        }

        button svg {
            cursor: pointer;
        }
    }
`;

export const Pagination = styled.div`
    display: flex;
    justify-content: center;
    margin-top: 20px;
`;

export const PageNumber = styled.button<{ active: boolean }>`
    display: flex;
    align-items: center;
    justify-content: center;
    width: 30px;
    height: 30px;
    border: none;
    background-color: ${({ active }) => (active ? '#333' : '#f0f0f0')};
    color: ${({ active }) => (active ? '#fff' : '#333')};
    font-size: 14px;
    cursor: pointer;
    margin: 0 5px;

    &:hover {
    background-color: ${({ active }) => (active ? '#333' : '#ccc')};
    }
`;

