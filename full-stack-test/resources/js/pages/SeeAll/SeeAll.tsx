import React, { useEffect, useState } from 'react';
import { useLocation } from 'react-router-dom';
import { Movie } from '../../components/GenreCarousel/GenreCarousel.style';
import Navbar from '../../components/Navbar/Navbar';
import Footer from '../../components/Footer/Footer';
import { MovieList, MovieItem, Container, Pagination, PageNumber } from './SeeAll.styles';

interface Movie {
  id: number;
  title: string;
  poster_path: string;
  genre_ids: number[];
  vote_average: number;
}

const SeeAll: React.FC = () => {
  const [movies, setMovies] = useState<Movie[]>([]);
  const [currentPage, setCurrentPage] = useState(1); // Estado para armazenar a página atual
  const [moviesPerPage] = useState(12); // Quantidade fixa de filmes por página
  const location = useLocation();

  useEffect(() => {
    const searchParams = new URLSearchParams(location.search);
    const moviesParam = searchParams.get('movies');
    if (moviesParam) {
      const parsedMovies = JSON.parse(decodeURIComponent(moviesParam));
      setMovies(parsedMovies);
    }
  }, [location.search]);

  console.log(movies);

  const imagePath = "https://image.tmdb.org/t/p/w500";

  // Lógica para calcular os índices de início e fim dos filmes a serem exibidos na página atual
  const indexOfLastMovie = currentPage * moviesPerPage;
  const indexOfFirstMovie = indexOfLastMovie - moviesPerPage;
  const currentMovies = movies.slice(indexOfFirstMovie, indexOfLastMovie);

  // Função para alterar a página atual
  const paginate = (pageNumber: number) => {
    setCurrentPage(pageNumber);
    smoothScrollTop(); // Chama a função de scroll suave para o topo
  };

  const smoothScrollTop = () => {
    const c = document.documentElement.scrollTop || document.body.scrollTop;
    if (c > 0) {
      window.requestAnimationFrame(smoothScrollTop);
      window.scrollTo(0, c - c / 8);
    }
  };

  return (
    <>
      <Navbar />
      <Container>
        <MovieList>
          {currentMovies.map(movie => (
            <MovieItem key={movie.id}>
              <img src={`${imagePath}${movie.poster_path}`} alt={movie.title} />
              <h3>{movie.title}</h3>
              <div>
                <button>em breve</button>
                <p>Nota: {movie.vote_average.toFixed(1)}</p>
              </div>
            </MovieItem>
          ))}
        </MovieList>
        <Pagination>
          {Array.from({ length: Math.ceil(movies.length / moviesPerPage) }, (_, index) => (
            <PageNumber
              key={index + 1}
              onClick={() => paginate(index + 1)}
              active={index + 1 === currentPage}
            >
              {index + 1}
            </PageNumber>
          ))}
        </Pagination>
      </Container>
      <Footer />
    </>
  );
};

export default SeeAll;
