import styled from 'styled-components';
import colors from '../../variables/Colors';

export const LoginContainer = styled.section`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    height: 100vh;
`;

export const LoginForm = styled.form`
    display: flex;
    flex-direction: column;
    align-items: center;
    width: 300px;
    padding: 20px;
    border: 1px solid #ccc;
    border-radius: 8px;

    a {
        color: ${colors.main_color};
        cursor: pointer;
    }
`;

export const FormTitle = styled.h1`
    margin-bottom: 20px;
`;

export const FormInput = styled.input`
    width: 100%;
    padding: 10px;
    margin-bottom: 10px;
    margin-top: 5px;
    border: 1px solid #000;
    border-radius: 8px;
`;

export const FormButton = styled.button`
    width: 100%;
    padding: 10px 20px;
    background-color: ${colors.main_color};
    color: #fff;
    border: none;
    border-radius: 4px;
    cursor: pointer;

    &:hover {
    background-color: ${colors.btn_hover};
    }
`;

export const ErrorMessage = styled.p`
    background: ${colors.error_color};
    margin-bottom: 12px;
    padding: 4px 8px;
    border-radius: 0px;
    font-size: 12px;
`

export const Message = styled.p`
    font-size: 14px;
    padding-top: 12px;
`