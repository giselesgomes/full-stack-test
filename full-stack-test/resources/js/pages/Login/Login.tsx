import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { LoginContainer, LoginForm, FormTitle, FormInput, FormButton, ErrorMessage, Message } from './Login.style';
import Footer from '../../components/Footer/Footer';
import axios from 'axios';

const Login: React.FC = () => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [errorMessage, setErrorMessage] = useState('');

    const navigate = useNavigate();

    const newUser = () => {
        navigate("/register/");
    };

    const handleLogin = async (e: React.FormEvent) => {
        e.preventDefault();

        try {
            const response = await axios.post('/api/login', {
                email,
                password,
            });
            console.log('login successful');
            navigate("/home");
        } catch (error) {
            console.error('login failed');
            setErrorMessage('Credenciais inválidas. Por favor, verifique seu e-mail e senha.');
        }
    };

    return (
        <>
            <LoginContainer>
                <FormTitle>Login:</FormTitle>
                <LoginForm onSubmit={handleLogin}>
                    <label>
                        E-mail:
                        <FormInput
                            type="email"
                            value={email}
                            onChange={(e) => setEmail(e.target.value)}
                        />
                    </label>
                    <label>
                        Senha:
                        <FormInput
                            type="password"
                            value={password}
                            onChange={(e) => setPassword(e.target.value)}
                        />
                    </label>
                    {errorMessage && <ErrorMessage>{errorMessage}</ErrorMessage>}
                    <FormButton type="submit">Entrar</FormButton>
                    <Message>Não tem cadastro? <a onClick={newUser}>Cadastre-se</a></Message>
                </LoginForm>
            </LoginContainer>
            <Footer></Footer>
        </>
    );
};

export default Login;
