import './bootstrap';
import React from 'react';
import { createInertiaApp } from '@inertiajs/react'
import { createRoot } from 'react-dom/client'
import { BrowserRouter, Routes, Route } from 'react-router-dom'

import SeeAll from './pages/SeeAll/SeeAll'
import Login from './pages/Login/Login'
import Users from './pages/Users/Users';
import RegisterUser from './pages/RegisterUser/RegisterUser';
import EditUser from './pages/EditUser/EditUser';
import Home from './pages/Home/Home';

createInertiaApp({
  resolve: name => {
    const pages = import.meta.glob('./pages/**/*.tsx', { eager: true })
    return pages[`./pages/${name}.tsx`]
  },
  setup({ el, App, props }) {
    createRoot(el).render(
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<App {...props} />} />
          <Route path="/home" element={<Home />} />
          <Route path="/see-all" element={<SeeAll />} />
          <Route path="/users" element={<Users />} />
          <Route path="/register" element={<RegisterUser />} />
          <Route path="/users/edit/:id" element={<EditUser />} />
        </Routes>
      </BrowserRouter>
    );
  },
});
