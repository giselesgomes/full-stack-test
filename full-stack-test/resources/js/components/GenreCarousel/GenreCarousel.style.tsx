import styled from "styled-components";
import colors from "../../variables/Colors";
import { Link } from 'react-router-dom';

export const Container = styled.section`
    padding: 2rem;

    h2 {
        font-size: 2rem;
        padding: 1rem 0;
    }

    img {
        border-radius: 1rem;
        width: 95%;
        height: auto;
        transition: all 0.3s;
    }

    @media (min-width: 480px) {
        img {
            width: 100%;
        }
    }

    @media (min-width: 600px) {
        img {
            width: 95%;
            max-width: 430px;
        }
    }

    @media (min-width: 1024px) {
        img {
            max-width: 450px;
        }
    }

    img:hover {
        transform: scale(1.01);
    }
    

    h3 {
        text-align: center;
        padding: 1rem 0.5rem;
        min-height: 78px; 
    }

`;

export const Movie = styled.div`
    displey: flex;
    flex-direction: column;
    align-items: center;
`

export const Genres = styled.ul`
    display: flex;
    column-gap: 10px;
    justify-content: center;

    li {
        list-style: none;
        background: ${colors.main_color};
        border-radius: 1rem;
        padding: 0 10px;
        color: black;
        font-size: 0.75rem;
    }
`
export const StyledLink = styled(Link)`
    background: none;
    border: none;
    font-size: 1rem;
    color: #FFF;
    font-weight: 600;
`;

export const CarouselHeader = styled.div`
    display: flex;
    column-gap: 2rem;
    align-items: center;

    button {
        color: ${colors.main_color};
        background: none;
        border: none;
        font-size: 1rem;
        text-decoration: underline;
        font-weight: 600;
        cursor: pointer;
    }

    button:hover {
        font-style: italic;
    }
`