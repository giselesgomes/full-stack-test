import React from 'react';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import { Container, Genres, Movie, CarouselHeader, StyledLink } from './GenreCarousel.style';

interface Movie {
    id: number;
    title: string;
    name: string;
    poster_path: string;
    genre_ids: number[];
}

interface Genre {
    id: number;
    name: string;
}

interface CarouselProps {
    movies: Movie[];
    genres: Genre[];
    title: string;
    carouselId: string;
}

const GenreCarousel: React.FC<CarouselProps> = ({ movies, genres, title }) => {
    const imagePath = "https://image.tmdb.org/t/p/w500";

    const getGenreNames = (genreIds: number[]) => {
        const filteredGenres = genres.filter(genre => genreIds.includes(genre.id));
        const genreNames = filteredGenres.slice(0, 2).map(genre => genre.name);
        return genreNames.map(genreName => <li key={genreName}>{genreName}</li>);
    };

    const settings = {
        infinite: true,
        speed: 1000,
        slidesToShow: 4,
        slidesToScroll: 2,
        initialSlide: 0,
        className: "center",
        centerMode: true,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    centerMode: false,
                    slidesToShow: 3,
                    slidesToScroll: 1,
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    initialSlide: 2,
                    centerPadding: "18px",
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    centerPadding: "10px",
                }
            }
        ]
    };

    return (
        <Container>
            <CarouselHeader>
                <h2>{title}</h2>
                <StyledLink to={`/see-all?movies=${encodeURIComponent(JSON.stringify(movies))}`}>
                    ver todos
                </StyledLink>
            </CarouselHeader>
            {movies.length > 0 ? (
                <Slider {...settings}>
                    {movies.map(movie => (
                        <div key={movie.id}>
                            <Movie>
                                <img src={`${imagePath}${movie.poster_path}`} alt={movie.title} />
                                <h3>{movie.title ? movie.title : movie.name}</h3>
                                <Genres>{getGenreNames(movie.genre_ids)}</Genres>
                            </Movie>
                        </div>
                    ))}
                </Slider>
            ) : (
                <p>Carregando filmes...</p>
            )}
        </Container>
    );
};

export default GenreCarousel;
