import React from 'react';
import { useNavigate } from 'react-router-dom';
import { NavWrapper, NavList, NavItem, NavLink } from './Navbar.style';
import { BsHouseDoorFill, BsPersonLinesFill } from "react-icons/bs";
const Navbar: React.FC = () => {

    const navigate = useNavigate();

    const UsersList = () => {
        navigate("/users");
    };

    const Home = () => {
        navigate("/home/");
    };
    return (
        <NavWrapper>
            <NavList>
                <NavItem>
                    <NavLink onClick={Home}>
                        <BsHouseDoorFill />
                        <span>Home</span>
                        </NavLink>
                </NavItem>
                <NavItem>
                    <NavLink onClick={UsersList}>
                        <BsPersonLinesFill />
                        <span>Usuários</span>
                        </NavLink>
                </NavItem>
            </NavList>
        </NavWrapper>
    );
};

export default Navbar;
