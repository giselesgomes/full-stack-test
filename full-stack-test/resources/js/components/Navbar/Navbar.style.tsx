import styled from 'styled-components'
import colors from '../../variables/Colors';

export const NavWrapper = styled.nav`
    background-color: ${colors.main_color};
    padding: 16px;
`;

export const NavList = styled.ul`
    list-style-type: none;
    display: flex;
    justify-content: space-between;
    align-items: center;
    color: #000;
    padding: 0 2rem;
`;

export const NavItem = styled.li`
    padding: 12px;
    display: flex;
    column-gap: 12px;
`;

export const NavLink = styled.button`
    text-decoration: none;
    background: none;
    border: none;
    color: #000;
    font-size: 1rem;
    cursor: pointer;
    display: flex;
    flex-direction: inherit;
    align-items: center;
    font-weight: 600;

    span {
        padding-left: 5px;
    }

    &:hover {
        color: #000;
    }
`;

