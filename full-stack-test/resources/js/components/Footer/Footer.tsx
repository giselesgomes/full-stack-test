import React from 'react';
import { FooterContainer, FooterText, FooterLink } from './Footer.styled';

const Footer: React.FC = () => {
    return (
        <FooterContainer>
            <FooterText>
                Desenvolvido por <FooterLink target="_blank" href="https://github.com/giselegomes">Gisele Gomes</FooterLink>
            </FooterText>
        </FooterContainer>
    );
};

export default Footer;
