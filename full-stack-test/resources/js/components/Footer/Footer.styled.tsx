import styled from 'styled-components';
import colors from '../../variables/Colors';

export const FooterContainer = styled.footer`
    background-color: ${colors.main_color};
    padding: 20px;
    text-align: center;
    width: 100%;
`;

export const FooterText = styled.p`
    font-size: 14px;
    color: #000;
`;

export const FooterLink = styled.a`
    color: #333;
    text-decoration: none;

    &:hover {
        text-decoration: underline;
    }
`;
