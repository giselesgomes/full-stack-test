const colors = {
    bg_main_color: "#1e1e22",
    main_color: '#e96744',
    btn_hover: '#ec915b',
    error_color: '#d62828'
};

export default colors;
