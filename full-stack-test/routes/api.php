<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UsersController;

Route::post('/login', [UsersController::class, 'login']);
Route::post('/add_user', [UsersController::class, 'add_user']);
Route::get('/get_all_user', [UsersController::class, 'get_all_user']);
Route::get('/get_edit_user/{id}', [UsersController::class, 'get_edit_user']);
Route::post('/update_user/{id}', [UsersController::class, 'update_user']);
Route::get('/delete_user/{id}', [UsersController::class, 'delete_user']);

Route::middleware('auth:sanctum')->group(function () {
    Route::get('/user', function (Request $request) {
        return $request->user();
    });

    Route::get('/users', [UsersController::class, 'users'])->middleware('auth');

    Route::get('/home', function () {
        return 'Welcome to the home page';
    });

});

