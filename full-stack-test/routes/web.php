<?php

use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

Route::get('/', function () {
    return Inertia::render('Login/Login');
})->name('login');

// acesso com autenticação
Route::middleware(['web', 'auth'])->group(function () {
    Route::get('/see-all', function () {
        return Inertia::render('SeeAll/SeeAll');
    })->name('seeAll');
    
    Route::get('/see-all?{any?}', function () {
        return Inertia::render('SeeAll/SeeAll');
    })->name('seeAll')->where('any', '.*');

    Route::get('/users', function () {
        return Inertia::render('Users/Users');
    })->name('users');
    
    Route::get('/users/edit/{id}', function () {
        return Inertia::render('EditUser/EditUser');
    })->name('editUser');

    Route::get('/home', function () {
        return Inertia::render('Home/Home');
    })->name('home');
});

Route::get('/register', function () {
    return Inertia::render('RegisterUser/RegisterUser');
})->name('register');

// qualquer rota redireciona para login
Route::fallback(function () {
    return redirect()->route('login');
});