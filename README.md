# Full Stack Teste

Teste Full Stack para a vaga de Front-end na Watch :orange_heart:

## Tecnologias Utilizadas
- React
- Laravel
- MySQL

## Pré-requisitos
- Node.js (https://nodejs.org)
- PHP (https://www.php.net)
- Composer (https://getcomposer.org)
- MySQL (https://www.mysql.com)

## API
Para fazer as requisições à API The Movie Database (https://developer.themoviedb.org/docs), é necessário utilizar um token na URL. Neste projeto, o token foi incluído nos commits para facilitar a visualização.

## Configuração do Ambiente

1. Depois de clonar o projeto, navegue até o diretório do projeto `full-stack-test`:
~~~
cd full-stack-test
~~~

2. Instale o composer:
~~~
composer install
~~~

3. O composer gerou o arquivo `.env.example`. Faça uma cópia do arquivo e renomeie-o para `.env`.
 
4. Abra o arquivo `.env` e insira as informações do banco de dados específicas do seu ambiente local. Certifique-se de preencher corretamente os valores das seguintes variáveis:
~~~
DB_DATABASE=seu_banco_de_dados
DB_USERNAME=seu_usuario
DB_PASSWORD=sua_senha
~~~
<i>substitua `seu_banco_de_dados`, `seu_usuario` e `sua_senha` pelas informações corretas do seu banco de dados local.</i>

5. Ainda dentro do diretório full-stack-test/full-stack-test, execute o comando:
~~~
npm install
~~~

6. Execute o Laravel com o comando:
~~~
php artisan serve
~~~ 
7. A aplicação abrirá na porta http://127.0.0.1:8000/

## Funcionalidades

### Login: 
- É acessada publicamente.
- Possui link para cadastro de novo usuário.
- Após realizar o login, o usuário é redirecionado para a Home.

### Cadastro de usuário
- É acessada publicamente.
- O usuário pode fazer o cadastro inserindo nome, e-mail e senha.
- Após o cadastro realizado, o usuário é redirecionado para a página de login.

### Home
- É acessada apenas pelo usuário autenticado.
- Possui uma grade com carrosséis de filmes separados por gênero.
- Há um link para as páginas de categorias de filmes, onde é possível ver todos os filmes por gênero.
- No Navbar, há um link para acessar a lista de usuários.

### Ver todos
- É acessada apenas pelo usuário autenticado.
- Mostra apenas os filmes do gênero escolhido.

### Usuários
- É acessada apenas pelo usuário autenticado.
- Mostra todos os usuários cadastrados.
- É possível editar, excluir e adicionar um novo usuário.

  ### Editar usuário: 
   - É acessada apenas pelo usuário autenticado.
   - Abre um formulário para editar o usuário selecionado.

  ### Excluir usuário:
    - Exclui o usuário selecionado.
   
  ### Adicionar novo usuário:
   - Abre a página de cadastro de usuários.